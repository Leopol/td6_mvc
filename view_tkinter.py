#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Feb 2021
Man-machine and MVC graphic interfaces (Model View Controller) --> VIEW
"""
from __future__ import absolute_import
# Importing the tkinter library to make the graphical interface
from tkinter import Tk, Entry, Label, StringVar, Button


class View(Tk):
    """
    This class defines here methods of display, graphics rendering and layout.
    This is a visual representation of the model.
    The graphical interface here is Tkinter.
    """

    def __init__(self, controller):
        """
        This initialization function calls the Tkinter initialization function
        and also establishes a link with the controller (especially for
        functions called by buttons).
        """
        super().__init__()
        self.controller = controller
        # Creation of dictionaries
        self.widgets_labs = {}
        self.widgets_entry = {}
        self.widgets_button = {}
        self.dico = {}
        # Entry for the output file
        var_path = StringVar()
        self.entry_path = Entry(self, textvariable=var_path, width=30)
        self.entry_path.grid(row=1, column=1, sticky="W")


    def clear_fields(self):
        """ Function that clears the fields.
        The label (Last name, First name, Phone...) is associated here as
        a key and the entry field as a value.
        """
        # For the key and value of our input fields (each)
        for key, value in self.widgets_entry.items():
            # The character 0 is deleted/deleted until the end of
            # the input field in each of the values.
            value.delete(0,'end')


    def main_view(self):
        """
        Main design and display function of the Tkinter graphical interface.
        Here we try to solve the redundancy of the codes/commands we make with
        the creation of the tkinter interface. In fact, by placing all our
        titles in a list, we browse it and store the labels and entries in a
        dictionary (the for loops).
        """
        # Creation of the main window
        self.title('PHONE BOOK 2021')

        # Creation of an information label
        message = 'Please fill in the fields to build your directory:'
        info = Label(self, text=message, font='Arial 15')
        info.grid(row=0, column=0)

        # Label for the output file
        message_path = 'Please enter the path where you want to create your phone book'
        file_path = Label(self, text=message_path, font='Arial 12')
        file_path.grid(row=1, column=0)


        # Creation of label and button names
        ids = ["Last name", "First name", "Phone", "Address", "City"]
        butt = ["Insert", "Search", "Delete"]

        # Creation of counters which allow positioning on the interface
        line, col = 4, 1

        # For items in the ids list which contains the labels
        for idi in ids:
            # Definitin of labels
            lab = Label(self, text=idi, font='Ubuntu 15')
            self.widgets_labs[idi] = lab
            lab.grid(row=line, column=0, sticky="E")

            # Input definition
            var = StringVar()
            entry = Entry(self, textvariable=var, width=30)
            self.widgets_entry[idi] = entry
            entry.grid(row=line, column=1, sticky="W")

            line +=3

        # For items in the butt list which contains the buttons
        for idi in butt:
            # Button definition
            button = Button(self, text=idi, font='Ubuntu 15')
            self.widgets_button[idi]=button
            button.grid(row=line, column=col)

            col +=1

        # Buttons and their actions on the data (function call)
        self.widgets_button["Insert"].config(command = self.controller.get_fields)
        self.widgets_button["Search"].config(command = self.controller.search_existing)
        self.widgets_button["Delete"].config(command = self.controller.remove)

        # Displaying the main window
        self.mainloop()
