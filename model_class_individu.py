#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Feb 2021
Man-machine and MVC graphic interfaces (Model View Controller) --> MODEL
"""

class Individu:
    """ Definition of an individual's characters.
    This class is here the model, it manages the data of our application.
    It retrieves and stores the information which is then processed/used by
    the controller.
    """

    def __init__(self, lastname, firstname, phone, address, city):
        """
        Enumeration of variables, characters of an individual.
        And initialization.
        """
        self.lastname = lastname
        self.firstname = firstname
        self.phone = phone
        self.address = address
        self.city = city


    def __str__(self):
        """
        Display function, which establishes a format for
        information characterizing an "individual".
        """
        lname = "Lastname:" + self.lastname + " / "
        fname = "Firstname:" + self.firstname + " / "
        pho = "Phone:" + self.phone + " / "
        addr = "Address:" + self.address + " / "
        cit = "City:" + self.city
        return str(lname + fname + pho + addr + cit)
