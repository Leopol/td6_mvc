#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Feb 2021
Man-machine and MVC graphic interfaces (Model View Controller) --> CONTROLLER
"""
from __future__ import absolute_import
# Import of necessary files, classes and libraries
from tkinter import messagebox
from model_class_individu import Individu
from view_tkinter import View


class DirectoryManagement:
    """
    This class is here the controller, it manages the management, the
    directives applied to the data (insertion, deletion, search).
    It is directly linked to the view in order to be able to respond to
    requests from the user who interacts through the graphical interface.
    """

    def __init__(self):
        """
        Initialization function taking into account the data inserted in the view.
        """
        self.view = View(self)


    def main_controller(self):
        """
        Main function of the controller which calls on the main function of
        the view in order to display the interface and to retrieve the data
        in order to carry out calculations, analysis methods.
        """
        self.view.main_view()


    def output_file(self):
        """
        Function that builds the output file. We get the phone book in a .txt file.
        """
        lastname = self.view.widgets_entry["Last name"].get()
        file_phone_book = open(str(self.view.entry_path.get()),"w")
        for lastname in self.view.dico:
            content = lastname + '\t' + str(self.view.dico[lastname]) + '\n'
            file_phone_book.write(content)
        file_phone_book.close()


    def known_info(self):
        """
        If the individual has already been entered, this function allows you
        to fill in the fields with the known information.
        """
        lastname = self.view.widgets_entry["Last name"].get()
        individu = self.view.dico[lastname]
        # self.view.widgets_entry["Last name"].insert(0, individu.lastname)
        self.view.widgets_entry["First name"].insert(0, individu.firstname)
        self.view.widgets_entry["Phone"].insert(0, individu.phone)
        self.view.widgets_entry["Address"].insert(0, individu.address)
        self.view.widgets_entry["City"].insert(0, individu.city)


    def get_fields(self):
        """
        Insertion function, which builds the directory according to
        the data filled in by the user.
        """
        # Retrieval of data entered in the fields by the user and association with variables.
        lastname = self.view.widgets_entry["Last name"].get()
        firstname = self.view.widgets_entry["First name"].get()
        phone = self.view.widgets_entry["Phone"].get()
        address = self.view.widgets_entry["Address"].get()
        city = self.view.widgets_entry["City"].get()
        # Creation of a variable that takes into account the different fields
        # in order to create an "individual".
        one = Individu(lastname, firstname, phone, address, city)
        if lastname == "":
            messagebox.showwarning("Warning",'You must fill in the lastname field.')
        elif lastname not in self.view.dico:
            self.view.dico[lastname] = one
            messagebox.showinfo("Information",'Your individual has been listed in the directory.')
            # Writing in the output file, filling the phone book
            self.output_file()
            # Clear fields after treatment
            self.view.clear_fields()
        else:
            messagebox.showwarning("Warning",'The name entered already exists.')
            # Clear fields after treatment
            self.view.clear_fields()


    def search_existing(self):
        """ Search the phone book if the name already exists. """
        # Variable of the lastname that retrieves the entered data.
        lastname = self.view.widgets_entry["Last name"].get()

        # If the lastname is already present in the phone book, the dictionary as a key.
        if lastname in self.view.dico.keys():
            # Displays a message and information if it has already been entered.
            text = 'The name entered already exists.\nHere is the information we have:\n'
            messagebox.showinfo("Information", text)
            # Call of the function to fill the fields with known information.
            self.known_info()
            # To view the information already entered on the individual in the pop'up
            ##messagebox.showinfo("Information", text + Individu.__str__(self.view.dico[lastname]))

        # Otherwise display a message to add the data (insert button).
        else:
            text_one = 'The name entered is not found in the phone book.\n'
            text_two = 'You can add it by clicking on the "Insert" button.'
            messagebox.showinfo("Information", text_one + text_two)


    def remove(self):
        """ Function that deletes information from the phone book. """
        # Variable of the lastname that retrieves the entered data that you want to delete.
        lastname = self.view.widgets_entry["Last name"].get()
        # If the lastname is already present in the phone book, the dictionary as a key.
        if lastname in self.view.dico.keys():
            # Display a message to inform that data has been deleted.
            messagebox.showinfo("Information", 'The entered information is deleted.')
            # Deletion command
            del self.view.dico[lastname]
        # Writing in the output file, filling the phone book
        self.output_file()
        # Clear fields after treatment
        self.view.clear_fields()


if __name__ == "__main__":
    # The file is here the main script, it calls itself starting with
    # its main() function.
    directory = DirectoryManagement()
    directory.main_controller()
